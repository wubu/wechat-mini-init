// components/tabs/tabs.js
Component({

	/**
	 * 定义组件间的关系
	 */
	relations: {
		'./pane': {
			type: 'child', // 关联的目标节点应为子节点

			/**
			 * 插入时执行，target是该节点实例对象，触发在该节点attached生命周期之后
			 * @param target
			 */
			linked: function (target) {
				this.data.titleItems.push(target.data);
				console.log('tabs linked: ', target)
			},

			/**
			 * 移动后执行，target是该节点实例对象，触发在该节点moved生命周期之后
			 * @param target
			 */
			linkChanged: function (target) {
				console.log('tabs linkChanged: ', target)
			},

			/**
			 * 移除时执行，target是该节点实例对象，触发在该节点detached生命周期之后
			 * @param target
			 */
			unlinked: function (target) {
				const index = this.data.titleItems.indexOf(target.data);
				if (index !== -1) this.data.titleItems.splice(index, 1);
				console.log('tabs unlinked: ', target)
			}
		}
	},

	/**
	 * 组件的属性列表
	 */
	properties: {
		/**
		 * 当前选中面板
		 */
		current: {
			type: Number,
			value: 0,
			observer: function (value) {
				this.setData({scrollIntoView: `tab-item-${value}`});
			}
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		titleItems: [],
		scrollIntoView: '',
	},

	/**
	 * 组件的方法列表
	 */
	methods: {

		/**
		 * 切换选项卡
		 * @param e
		 */
		onItemTap: function (e) {
			const index = e.currentTarget.dataset.index;
			this.setData({current: index});
			this.triggerEvent('change', {index: index});
		}

	},

	/**
	 * 渲染页面
	 */
	ready: function () {
		// var nodes = this.getRelationNodes('./pane')
		// console.log(nodes)
		this.setData({titleItems: this.data.titleItems});
	}
});