import listener from '../utils/listener.js';
import qs from '../utils/qs.js';

//app start
listener.once('app.init', (res) => {
	console.log('app start:', res);
});

//app exit
listener.once('app.exit', (res) => {
	console.log('app exit:', res);
});

//app show
listener.on('app.show', (res) => {
	console.log('app show:', res);
});

//app hide
listener.on('app.hide', (res) => {
	console.log('app hide:', res);
});

//app error
listener.on('app.error', (res) => {
	console.error('app error:', res);
});

//app page.notfound
listener.on('app.page.notfound', (res) => {
	console.error('app page.notfound:', res);
});

//初始化授权监听
listener.on('wx.userinfo.to', (res) => {
	if (getApp().globalData.isAuthing) return;
	getApp().globalData.isAuthing = true;

	const url = '/pages/user/auth/auth?' + qs.stringify(res, { encode: false, encodeValuesOnly: true });
	wx.navigateTo({ url });
});