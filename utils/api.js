import http from './http.js';
import {urls} from "../config";

/**
 * 封装常用的api
 */
export default {

	/**
	 * 获取用户信息
	 */
	getUserInfo(callback) {
		http.get(urls.member.info, data, callback);
	},

	/**
	 * 保存用户信息
	 * @param data
	 * @param callback
	 */
	setUserInfo(data, callback) {
		http.get(urls.member.save, data, callback);
	}

};