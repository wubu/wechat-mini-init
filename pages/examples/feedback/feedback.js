import http from '../../../utils/http';
import {urls} from "../../../config";

Page({
	data: {
		remark: "",
		remark_length: 0,
		wechat: "",
		email: "",
	},
	onLoad: function () {
	},
	onWechatInput: function (e) {
		this.setData({
			wechat: e.detail.value
		})
	},
	onEmailInput: function (e) {
		this.setData({
			email: e.detail.value
		});
	},
	onRemarkInput: function (e) {
		this.setData({
			remark: e.detail.value,
			remark_length: e.detail.value.length
		});
	},
	onSubmit: function () {
		if (!this.data.remark) {
			return wx.showModal({
				content: "请输入您宝贵的意见",
				showCancel: false
			});
		}
		const data = {
			app_version: "",
			wechat: this.data.wechat,
			remark: this.data.remark,
			system_info: wx.getSystemInfoSync(),
			email: this.data.email,
		};
		console.log(data);
		http.post(urls.public.feedback, data, () => {
			wx.navigateBack();
		});
	}
});