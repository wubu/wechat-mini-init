// pages/examples/tabs/tabs.js
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		current: 1
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {

	},

	/**
	 * 选项卡改变
	 * @param e
	 */
	onChange: function (e) {
		this.setData({current: e.detail.index});
	},

	/**
	 * 切换到上一个选项卡
	 * @param e
	 */
	onPrevTap: function () {
		let current = this.data.current;
		if (current > 0) current--;
		this.setData({current: current});
	},

	/**
	 * 切换到下一个选项卡
	 * @param e
	 */
	onNextTap: function () {
		let current = this.data.current;
		if (current < 7) current++;
		this.setData({current: current});
	}

});